/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_vlan.h"

#define ME "vlan_mib"
#define MOD "vlan"
#define UNUSED __attribute__((unused))
#define string_empty(X) ((X == NULL) || (*X == '\0'))

typedef struct {
    amxc_htable_it_t it;
    amxs_sync_ctx_t* ctx;
} vlan_ctx_t;

static amxc_htable_t vlan_htable;

static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }

    return path;
}

static void vlan_ctx_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    if(it != NULL) {
        vlan_ctx_t* vlan = amxc_container_of(it, vlan_ctx_t, it);
        amxs_sync_ctx_stop_sync(vlan->ctx);
        amxs_sync_ctx_delete(&vlan->ctx);
        amxc_htable_it_clean(&vlan->it, NULL);
        free(vlan);
    }
}

static vlan_ctx_t* try_to_recycle_vlan_ctx(const char* netmodel_intf) {
    vlan_ctx_t* vlan = NULL;
    amxc_htable_it_t* it = NULL;

    it = amxc_htable_get(&vlan_htable, netmodel_intf);
    if(it != NULL) {
        vlan = amxc_container_of(it, vlan_ctx_t, it);
        amxs_sync_ctx_stop_sync(vlan->ctx);
        amxs_sync_ctx_delete(&vlan->ctx);
    }

    if(vlan == NULL) {
        vlan = calloc(1, sizeof(vlan_ctx_t));
        amxc_htable_insert(&vlan_htable, netmodel_intf, &vlan->it);
    }

    return vlan;
}

static amxs_status_t vlan_action_cb(UNUSED const amxs_sync_entry_t* sync_entry,
                                    UNUSED amxs_sync_direction_t direction,
                                    amxc_var_t* data,
                                    UNUSED void* priv) {
    const char* vlan_path = NULL;
    const char* netmodel_intf = GET_CHAR(data, "path");
    vlan_ctx_t* vlan = NULL;
    amxs_status_t ret = amxs_status_unknown_error;
    amxc_string_t path;

    amxc_string_init(&path, 0);

    vlan = try_to_recycle_vlan_ctx(netmodel_intf);
    when_null(vlan, exit);

    vlan_path = skip_prefix(GETP_CHAR(data, "parameters.VLAN"));
    amxc_string_set(&path, vlan_path);
    if(vlan_path[strlen(vlan_path) - 1] != '.') {
        amxc_string_append(&path, ".", 1);
    }

    ret = amxs_sync_ctx_new(&vlan->ctx, amxc_string_get(&path, 0), netmodel_intf, AMXS_SYNC_ONLY_A_TO_B);
    ret |= amxs_sync_ctx_add_new_copy_param(vlan->ctx, "VLANID", "VLANID", AMXS_SYNC_DEFAULT);
    if(ret != amxs_status_ok) {
        SAH_TRACEZ_WARNING(ME, "Failed to create sync context for %s and %s, reason: %d",
                           amxc_string_get(&path, 0), netmodel_intf, ret);
        goto exit;
    }

    ret = amxs_sync_ctx_start_sync(vlan->ctx);
    if(ret != amxs_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to start sync for %s and %s, reason: %d",
                         amxc_string_get(&path, 0), netmodel_intf, ret);
        goto exit;
    }

exit:
    if(ret != amxs_status_ok) {
        vlan_ctx_delete(NULL, (vlan != NULL) ? &vlan->it : NULL);
    }

    amxc_string_clean(&path);

    return amxs_status_ok;
}

static amxs_status_t add_vlanport_parameters(amxs_sync_ctx_t* ctx) {
    return amxs_sync_ctx_add_new_param(ctx, "VLAN", "VLAN", AMXS_SYNC_ONLY_A_TO_B, amxs_sync_param_copy_trans_cb, vlan_action_cb, NULL);
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_unknown_error;

    status = amxs_sync_ctx_add_new_copy_param(ctx, "VLANID", "VLANID", AMXS_SYNC_DEFAULT);

    return status;
}

static netmodel_mib_t* netmodel_config(const char* netmodel_intf) {
    static netmodel_mib_t default_mib = {
        .name = ME,
        .root = "Ethernet.",
        .add_sync_parameters = add_parameters,
    };
    static netmodel_mib_t bridgevlan_mib = {
        .name = ME,
        .root = "Bridging.",
        .interfaces_parameter = "Port",
        .flags = NETMODEL_MIB_LINK_AS_LOWER_LAYER,
        .add_sync_parameters = add_vlanport_parameters,
    };
    netmodel_mib_t* mib = &default_mib;

    if(string_empty(netmodel_intf)) {
        SAH_TRACEZ_WARNING(ME, "Using default libnetmodel configuration");
        goto exit;
    }

    if(strstr(netmodel_intf, "bridgevlan-") != NULL) {
        mib = &bridgevlan_mib;
    }
    if(strstr(netmodel_intf, "bridge-") != NULL) {
        mib = NULL; // instances from Bridging.Bridge.*.Port.*. need the vlan flag but not the mib
    }

exit:
    return mib;
}

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_mib_t* mib = netmodel_config(GET_CHAR(args, "object"));
    if(mib != NULL) {
        netmodel_subscribe(args, mib);
    }

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_mib_t* mib = netmodel_config(GET_CHAR(args, "object"));
    if(mib != NULL) {
        netmodel_unsubscribe(args, mib);
    }

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    amxc_htable_init(&vlan_htable, 5);
    netmodel_initialize();

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    amxc_htable_clean(&vlan_htable, vlan_ctx_delete);
    netmodel_cleanup();

    return 0;
}
