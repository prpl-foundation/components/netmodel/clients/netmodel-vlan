# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.2 - 2024-11-21(15:18:03 +0000)

### Other

- [NetModel] Vlan mib fails to synchronize the VLANID for

## Release v1.0.1 - 2023-03-24(16:47:15 +0000)

### Fixes

- Fix netmodel-vlan unit tests

## Release v1.0.0 - 2022-02-28(10:51:33 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.1.1 - 2022-02-14(17:31:36 +0000)

### Fixes

- only the NetDev client should synchronize NetDevName

## Release v0.1.0 - 2022-02-02(11:42:50 +0000)

### New

- Integrate support for bridging in NetModel

